-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`oblast`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`oblast` (
  `code_oblast` INT(11) NOT NULL,
  `oblast` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`code_oblast`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`city` (
  `city` VARCHAR(45) NOT NULL,
  `oblast_code_oblast` INT(11) NOT NULL,
  PRIMARY KEY (`city`),
  INDEX `fk_city_oblast1_idx` (`oblast_code_oblast` ASC) VISIBLE,
  CONSTRAINT `fk_city_oblast1`
    FOREIGN KEY (`oblast_code_oblast`)
    REFERENCES `mydb`.`oblast` (`code_oblast`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`debt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`debt` (
  `debt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`debt`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`graduated_vnz`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`graduated_vnz` (
  `id` INT(11) NOT NULL,
  `vnz_name` VARCHAR(45) NULL DEFAULT NULL,
  `phone_number` VARCHAR(45) NULL DEFAULT NULL,
  `dirctor` VARCHAR(45) NULL DEFAULT NULL,
  `city_city` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_graduated_vnz_city1_idx` (`city_city` ASC) VISIBLE,
  CONSTRAINT `fk_graduated_vnz_city1`
    FOREIGN KEY (`city_city`)
    REFERENCES `mydb`.`city` (`city`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`group`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`group` (
  `group_number` INT(11) NOT NULL,
  `groupe_name` VARCHAR(45) NULL DEFAULT NULL,
  `year_of_enroll` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`group_number`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`student_has_debt`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student_has_debt` (
  `student_student_card` INT(11) NOT NULL,
  `debt_debt` VARCHAR(45) NOT NULL,
  INDEX `fk_student_has_debt_debt1_idx` (`debt_debt` ASC) VISIBLE,
  INDEX `fk_student_has_debt_student1_idx` (`student_student_card` ASC) VISIBLE,
  CONSTRAINT `fk_student_has_debt_debt1`
    FOREIGN KEY (`debt_debt`)
    REFERENCES `mydb`.`debt` (`debt`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student` (
  `student_card` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NULL,
  `po_batkove` VARCHAR(45) NULL,
  `date_of_birth` VARCHAR(45) NULL,
  `date_of_enroll` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `graduated_vnz_id` INT(11) NOT NULL,
  `city_city` VARCHAR(45) NOT NULL,
  `group_group_number` INT(11) NOT NULL,
  PRIMARY KEY (`student_card`),
  INDEX `fk_student_graduated_vnz1_idx` (`graduated_vnz_id` ASC) VISIBLE,
  INDEX `fk_student_city1_idx` (`city_city` ASC) VISIBLE,
  INDEX `fk_student_group1_idx` (`group_group_number` ASC) VISIBLE,
  CONSTRAINT `fk_student_graduated_vnz1`
    FOREIGN KEY (`graduated_vnz_id`)
    REFERENCES `mydb`.`graduated_vnz` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_city1`
    FOREIGN KEY (`city_city`)
    REFERENCES `mydb`.`city` (`city`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_group_number`)
    REFERENCES `mydb`.`group` (`group_number`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`student_has_debt1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`student_has_debt1` (
  `student_student_card` INT NOT NULL,
  `debt_debt` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`student_student_card`, `debt_debt`),
  INDEX `fk_student_has_debt1_debt1_idx` (`debt_debt` ASC) VISIBLE,
  INDEX `fk_student_has_debt1_student1_idx` (`student_student_card` ASC) VISIBLE,
  CONSTRAINT `fk_student_has_debt1_student1`
    FOREIGN KEY (`student_student_card`)
    REFERENCES `mydb`.`student` (`student_card`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_student_has_debt1_debt1`
    FOREIGN KEY (`debt_debt`)
    REFERENCES `mydb`.`debt` (`debt`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
