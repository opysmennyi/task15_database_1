-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema students
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema home_work
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema home_work
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `home_work` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
USE `home_work` ;

-- -----------------------------------------------------
-- Table `home_work`.`gender`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `home_work`.`gender` (
  `gender` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`gender`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `home_work`.`family_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `home_work`.`family_tree` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `date_of_birth` VARCHAR(45) NOT NULL,
  `date_of_death` VARCHAR(45) NULL DEFAULT NULL,
  `place_of_birth` VARCHAR(45) NOT NULL,
  `place_of_death` VARCHAR(45) NULL DEFAULT NULL,
  `credit_card_number` VARCHAR(45) NULL DEFAULT NULL,
  `gender_id` INT(11) NOT NULL,
  `family_tree_id` INT(11) NOT NULL,
  `family_tree_gender_id` INT(11) NOT NULL,
  `gender_gender` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `gender_id`, `family_tree_id`, `family_tree_gender_id`, `gender_gender`),
  INDEX `fk_family_tree_family_tree1_idx` (`family_tree_id` ASC, `family_tree_gender_id` ASC) VISIBLE,
  INDEX `fk_family_tree_gender1_idx` (`gender_gender` ASC) VISIBLE,
  CONSTRAINT `fk_family_tree_family_tree1`
    FOREIGN KEY (`family_tree_id` , `family_tree_gender_id`)
    REFERENCES `home_work`.`family_tree` (`id` , `gender_id`),
  CONSTRAINT `fk_family_tree_gender1`
    FOREIGN KEY (`gender_gender`)
    REFERENCES `home_work`.`gender` (`gender`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `home_work`.`family_suputniki`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `home_work`.`family_suputniki` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `date_of_birth` VARCHAR(45) NOT NULL,
  `date_of_death` VARCHAR(45) NULL DEFAULT NULL,
  `place_of_birth` VARCHAR(45) NOT NULL,
  `place_of_death` VARCHAR(45) NULL DEFAULT NULL,
  `date_of_marriage` VARCHAR(45) NULL DEFAULT NULL,
  `gender_id` INT(11) NOT NULL,
  `family_tree_id` INT(11) NOT NULL,
  `family_tree_gender_id` INT(11) NOT NULL,
  `gender_gender` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`, `gender_id`, `gender_gender`),
  INDEX `fk_family_suputniki_family_tree1_idx` (`family_tree_id` ASC, `family_tree_gender_id` ASC) VISIBLE,
  INDEX `fk_family_suputniki_gender1_idx` (`gender_gender` ASC) VISIBLE,
  CONSTRAINT `fk_family_suputniki_family_tree1`
    FOREIGN KEY (`family_tree_id` , `family_tree_gender_id`)
    REFERENCES `home_work`.`family_tree` (`id` , `gender_id`),
  CONSTRAINT `fk_family_suputniki_gender1`
    FOREIGN KEY (`gender_gender`)
    REFERENCES `home_work`.`gender` (`gender`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `home_work`.`family_values`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `home_work`.`family_values` (
  `code` INT(11) NOT NULL AUTO_INCREMENT,
  `value_name` VARCHAR(45) NOT NULL,
  `estimated_cost` VARCHAR(45) NOT NULL,
  `max_cost` VARCHAR(45) NOT NULL,
  `min_cost` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`code`))
ENGINE = InnoDB
AUTO_INCREMENT = 3433
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `home_work`.`family_values_has_family_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `home_work`.`family_values_has_family_tree` (
  `family_values_id` INT(11) NOT NULL,
  `family_tree_id` INT(11) NOT NULL,
  PRIMARY KEY (`family_values_id`, `family_tree_id`),
  INDEX `fk_family_values_has_family_tree_family_tree1_idx` (`family_tree_id` ASC) VISIBLE,
  INDEX `fk_family_values_has_family_tree_family_values_idx` (`family_values_id` ASC) VISIBLE,
  CONSTRAINT `fk_family_values_has_family_tree_family_tree1`
    FOREIGN KEY (`family_tree_id`)
    REFERENCES `home_work`.`family_tree` (`id`),
  CONSTRAINT `fk_family_values_has_family_tree_family_values`
    FOREIGN KEY (`family_values_id`)
    REFERENCES `home_work`.`family_values` (`code`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
